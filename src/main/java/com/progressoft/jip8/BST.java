package com.progressoft.jip8;

public class BST {

    private BSTNode root;

    private class BSTNode {
        int data;
        BSTNode left, right;

        public BSTNode(int key) {
            data = key;
            left = null;
            right = null;
        }
    }

    private class Acceptance {
        BSTNode node;
        boolean accepted;

        public Acceptance(BSTNode node, boolean accepted) {
            this.node = node;
            this.accepted = accepted;
        }
    }


    public BST() {
        root = null;
    }

    public boolean accept(int key) {
        Acceptance acceptance = findThenInsert(root, key);
        root = acceptance.node;
        return acceptance.accepted;
    }

    public boolean contains(int key) {
        return find(root, key);
    }

    public int treeDepth() {
        return treeDepth(root);
    }

    public int depth(int key) {
        return depth(root, key);
    }

    public void print() {
        printTree(root);
    }

    private boolean find(BSTNode root, int key) {
        if (root == null)
            return false;
        if (root.data == key)
            return true;
        if (root.data > key)
            return find(root.left, key);
        return find(root.right, key);
    }

    private Acceptance findThenInsert(BSTNode node, int key) {
        if (node == null) {
            node = new BSTNode(key);
            return new Acceptance(node, true);
        }

        if (node.data == key) {
            return new Acceptance(node, false);
        }

        Acceptance acceptance;
        if (node.data < key) {
            acceptance = findThenInsert(node.right, key);
            node.right = acceptance.node;
        } else {
            acceptance = findThenInsert(node.left, key);
            node.left = acceptance.node;
        }
        return new Acceptance(node, acceptance.accepted);
    }

    private BSTNode insert(BSTNode node, int key) {
        if (node == null) {
            node = new BSTNode(key);
            return node;
        }

        if (node.data < key)
            node.right = insert(node.right, key);
        else
            node.left = insert(node.left, key);

        return node;
    }

    private int treeDepth(BSTNode node) {
        if (node == null)
            return 0;

        int l = treeDepth(node.left);
        int r = treeDepth(node.right);

        return Math.max(l, r) + 1;
    }

    private int depth(BSTNode root, int key) {
        if (root.data == key) {
            return 1;
        }

        if (root.data > key)
            return 1 + depth(root.left, key);
        else
            return 1 + depth(root.right, key);
    }

    private void printTree(BSTNode root) {
        if (root != null) {
            printTree(root.left);
            System.out.println(root.data);
            printTree(root.right);
        }
    }
}