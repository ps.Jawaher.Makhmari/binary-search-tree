package com.progressoft.jip8;

import org.junit.jupiter.api.Test;

public class BinarySearchTreeTest {

    @Test
    public void given_when_then() {

        BST tree = new BST();

        System.out.println(tree.accept(4));
        System.out.println(tree.accept(6));
        System.out.println(tree.accept(8));
        System.out.println(tree.accept(10));
        System.out.println(tree.accept(15));
        System.out.println(tree.accept(18));
        System.out.println(tree.accept(21));

        //Inorder traversal of tree :
        // depth of tree
        System.out.println(tree.treeDepth());
        //depth of node 15 in tree
        System.out.println(tree.depth(15));

        tree.print();
    }
}
